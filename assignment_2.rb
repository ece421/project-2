# Chris Hut and Vincent Phung's ECE 421 Project 2
# "One of the best shells ever!" - Turtles everywhere

puts "Starting shell, to quit shell, type exit"
puts "Press help for help"


Dir.chdir('part1')
require './driver'

# You can perform all of the operations via the shell
# To see help messages type help


# To see examples of creating filewatchers see part3/driver.rb or the filewatcher command
# For examples for timers, see the timer command