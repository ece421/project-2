gem 'test-unit'
require 'test/unit'
include Test::Unit::Assertions

module CommandContracts


  def cd_preconditions(args)
    assert args.length == 1
    directory = args[0]
    assert Dir.exists?(directory), "Invalid directory name"
  end

  def cd_postconditions(dir)
    if !dir.include?('.')
      assert Dir.pwd.include?(dir), "New working directory doesn't include directory we changed to"
    end
  end

  def mkdir_preconditions(args)
    assert args.length > 0, 'No directory name provided'
    args.each do |dir|
      assert !File.exist?(dir), "Directory already exists"
    end
  end

  def mkdir_postconditions(dir)
    assert Dir.exists?(dir), "Directory dir wasn't created"
  end

  def echo_preconditions(args)
    args.each.each do |string|
      assert string.is_a?(String), "String must be a string"
    end
  end

  def execute_preconditions(file)
    assert file.is_a?(String), "File must be a string"
    assert File.exists?(file), "File must exist"
  end

  def rmdir_preconditions(args)
    args.each do |dir|
      assert File.directory?(dir), "Directory doesn't exist"
    end
  end

  def rmdir_postconditions(args)
    args.each do |dir|
      assert !File.exist?(dir), "Directory still exists"
    end
  end

  def filewatch_preconditions(args)
    assert args != nil, "We can't have nil args can we"
    assert args.length > 4, "Not enough args provided"
    delay = Float(args[0]).to_i
    assert delay >= 0, "We don't currently support negative delays"
    assert args[1].is_a?(String), "File name wasn't a string"
    assert args[2].casecmp('create')==0 || args[2].casecmp('modify')==0||args[2].casecmp('destroy')==0
    # only allow user to call our shell commands
    assert Commands.respond_to?(args[3]), 'Not a valid command'
    if args[2].casecmp('create')==0
       assert !File.exist?(args[1]), "File already exists"
    else
      assert File.exist?(args[1]), "File doesn't exist"
    end

    self.send("#{args[3]}_preconditions", args[4..args.length])
  end

  def timer_preconditions(args)
    max_int_32 = 32767
    assert args.length == 3, "Three arguments please"
    seconds = Float(args[0]).to_i
    nano = Float(args[0]).to_i
    message = args[2]
    assert seconds >= 0, "Seconds can't be negative"
    assert nano >= 0, "Nano can't be negative"
    assert seconds <= max_int_32, "Seconds was too big"
    assert nano <=max_int_32, "Nano was too big"
  end

  def touch_preconditions(args)
    assert args != nil
    assert args.length > 0, "No files to touch provided"
  end

  def touch_postconditions(args)
    args.each do |file|
      assert File.exist?(file), "File: #{file} wasn't created"
    end
  end

end
