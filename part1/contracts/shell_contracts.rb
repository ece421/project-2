gem 'test-unit'
require 'test/unit'
include Test::Unit::Assertions
module ShellContracts

  def run_command_preconditions(command)
    assert command.is_a?(String)
  end

  def class_invariant
    assert @prompt != nil
  end

end
