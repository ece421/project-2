require '../part1/contracts/shell_contracts'
require '../part1/commands'
require 'shellwords'
require 'thread'


class Shell
  include ShellContracts
  include Commands
  include InputValidation

  attr_reader :prompt

  # Creates the shell with a prompt of $
  def initialize
    @prompt = '$ '
  end

  # Executes the command on a separate thread
  def execute(command)
    run_command_preconditions(command)
    class_invariant

    args = Shellwords.split(command)
    if shell_command_validation(args)
      child = Thread.new do
        begin
          Commands.send(args[0], args[1..args.length])
        rescue NoMethodError
          raise NotImplementedError.new(args[0] + " is not implemented")
        ensure
          class_invariant
        end
      end
      child.join
    end
    class_invariant
  end
end