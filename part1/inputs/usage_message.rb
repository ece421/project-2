
module UsageMessage

  def echo_help
    puts "Display a line of text"
    puts "echo <string>"
  end

  def mkdir_help
    puts "Create empty directory"
    puts "mkdir <directory>..."
  end

  def ls_help
    puts "List directory contents, ignores all arguments"
    puts "ls"
  end

  def pwd_help
    puts "Print name of current/working directory, ignores all arguments"
    puts "pwd"
  end

  def cd_help
    puts "Change directory"
    puts "cd <directory>"
  end

  def help_help
    puts "Display information about builtin commands"
    puts "help"
  end

  def date_help
    puts "Print the system date and time, ignores all arguments "
    puts "date"
  end

  def rmdir_help
    puts "Remove the directory(ies), if they are empty"
    puts 'rm <directory>...'
  end

  def filewatch_help
    puts "Create a file watcher that performs an action when a file is altered"
    puts "filewatcher <delay> <file> <alteration> <action> <action_args>"
    puts
    puts "<delay>        delay in seconds until action is performed"
    puts "<file>         file to create the watcher for"
    puts "<alteration>   type of file watcher: create, modify, or destroy"
    puts "<action>       action to perform, must be a vald shell command"
    puts "<action_args>  arguments for the action"
    puts
    puts "Create file watchers will continue to run until file is created"
    puts "Modify and delete watchers will continue to run until file is deleted"
  end

  def timer_help
    puts "Create a timer that displays a message after a delay"
    puts "timer <seconds> <nanoseconds> <message>"
  end

  def touch_help
    puts "Creates file <files>"
    puts "touch <files>..."
  end

end