require_relative 'usage_message'

# module for input validation
# check if args was --help, then print usage message
module InputValidation

  include UsageMessage

  def echo_validation(args)
    # any args are fine, we just put it all out there
    if args.length > 0
      if args[0] == '--help'
        echo_help
        return false
      end
    end
    true
  end

  def mkdir_validation(args)
    if args.length == 0
      puts "missing operand"
      mkdir_help
      return false
    end
    if args[0] == '--help'
      mkdir_help
      return false
    end
    args.each do |dir|
      if File.exist?(dir)
        puts "#{dir} already exists"
        mkdir_help
        return false
      end
    end
    true
  end

  def ls_validation(args)
    if args.length > 0
      if args[0] == '--help'
        ls_help
        return false
      end
    end
    true
  end

  def pwd_validation(args)
    # nothing to validate here
    if args.length > 0
      if args[0] == '--help'
        pwd_help
        return false
      end
    end
    true
  end

  def cd_validation(args)
    if args.length == 0 || args.length > 1
      cd_help
      return false
    end
    if args[0] == '--help'
      cd_help
      return false
    end
    if !File.exist?(args[0])
      puts "#{args[0]}: No such file or directory"
      cd_help
      return false
    end
    if !Dir.exists?(args[0])
      puts "#{args[0]}: is not a directory"
      cd_help
      return false
    end
    true
  end

  def help_validation(args)
    if args.length == 0
      return true
    end
    if args.length > 1
      help_help
      return false
    end
    if args[0] == '--help'
      help_help
      return false
    end
    if !Commands.respond_to?(args[0])
      puts "No help topics match '#{args[0]}'"
      help_help
      return false
    end
    true
  end

  def date_validation(args)
    # Don't care about args, we don't allow for setting of date
    if args.length > 0
      if args[0] == '--help'
        date_help
        return false
      end
    end
    true
  end

  def rmdir_validation(args)
    if args.length == 0
      rmdir_help
      return false
    end
    args.each do |dir|
      if !File.directory?(dir)
        puts "#{dir}: No such file or directory"
        rmdir_help
        return false
      end
    end
    true
  end

  def filewatch_validation(args)
    if args.length < 4
      filewatch_help
      return false
    end
    begin
      delay = Float(args[0]).to_i
    rescue ArgumentError
      puts "Delay must be an integer"
      return false
    end
    if delay < 0
      puts "Delay must be a positive integer"
      filewatch_help
      return false
    end
    if args[2].casecmp('create')!=0 && args[2].casecmp('modify')!=0 && args[2].casecmp('destroy')!=0
      puts "#{args[2]}: Invalid alteration"
      filewatch_help
      return false
    end
    if !Commands.respond_to?(args[3])
      puts "#{args[3]}: Not a valid command"
      filewatch_help
      return false
    end
    if args[2].casecmp('create')==0
      if File.exist?(args[1])
        puts "File: #{args[1]} already exists"
        filewatch_help
        return false
      end
    else
      if !File.exist?(args[1])
        puts "File: #{args[1]} does not exist"
        filewatch_help
        return false
      end
    end
    if !self.send("#{args[3]}_validation", args[4..args.length])
      filewatch_help
      return false
    end
    true
  end

  def timer_validation(args )
    max_int_32 = 32767
    if args.length != 3 || args[0] == '--help'
      timer_help
      return false
    end
    begin
      seconds = Float(args[0]).to_i
      nano_seconds = Float(args[0]).to_i
    rescue ArgumentError
      puts "Delay must be an integer"
      return false
    end
    if seconds < 0 || nano_seconds < 0
      puts "Delay must be a positive integer"
      filewatch_help
      return false
    end
    message = args[2]
    if seconds > max_int_32
      timer_help
      puts "Seconds is too large, max is #{max_int_32}"
      return false
    end
    if nano_seconds > max_int_32
      puts "error: nanoseconds is too big, max is #{max_int_32}"
      timer_help
      return false
    end
    true
  end

  def touch_validation(args)
    if args.length == 0 || args[0] == '--help'
      touch_help
      return false
    end
    true
  end

  def shell_command_validation(args)
    commands = %w(touch ls help mkdir rmdir filewatch echo pwd cd date timer)
    if args.length == 0 || !commands.include?(args[0])
      puts "#{args[0]}: command not found" unless args.length == 0
      return false
    end
    true
  end

end