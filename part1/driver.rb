require '../part1/shell'

shell = Shell.new

# This will just be a while(1)
# get command from shell
# do command via commands module
# if command was exit exit

loop do
  print shell.prompt

  begin
    raw_input = gets
  rescue Interrupt
    break
  end

  if raw_input == nil
    break
  end

  input = raw_input.chomp

  if input == 'exit'
    break
  end

  if input.length == 0
    next
  end

  begin
    shell.execute(input)
  rescue NotImplementedError
    puts "Command not found"
  end
end