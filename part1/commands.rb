require '../part1/contracts/command_contracts'
require File.dirname(__FILE__) + '/../part3/file_watcher'
require '../timer/actionTimer'
require '../part1/inputs/input_validation'

# module for all of our commands
module Commands

  class << self
    include CommandContracts
    include InputValidation
  end

  def Commands.echo(args)
    if echo_validation(args)
      echo_preconditions(args)
      puts args.join(" ")
    end
  end

  def Commands.mkdir(args)
    if mkdir_validation(args)
      mkdir_preconditions(args)

      begin
        args.each do |dir|
          Dir.mkdir(dir)
        end
      rescue Errno::ENOENT
        puts "Original directory doesn't exist"
        return
      rescue Errno::EACCES
        puts "You don't have permissions to put that there"
        return
      end
      mkdir_postconditions(args[0])
    end
  end

  def Commands.ls(args)
    if ls_validation(args)

      puts Dir.entries(Dir.pwd).sort

    end
  end

  def Commands.pwd(args)
    if pwd_validation(args)
      puts Dir.pwd
    end
  end

  def Commands.cd(args)
    if cd_validation(args)
      cd_preconditions(args)
      dir = args[0]
      if File.exist?(dir)
        Dir.chdir(dir)
        cd_postconditions(dir)
      end
    end
  end

  def Commands.help(args)
    if help_validation(args)
      puts "Commands: echo, mkdir, ls, pwd, help, date, rmdir, filewatch, timer, touch"
      puts "For command specific help: <command> --help"
    end
  end

  def Commands.date(args)
    if date_validation(args)
      puts Time.new.strftime("%a %b %d %H:%M:%S %Z %Y")
    end
  end

  def Commands.rmdir(args)
    if rmdir_validation(args)
      rmdir_preconditions(args)
      begin
        args.each do |dir|
          Dir.rmdir(dir)
        end
      rescue Errno::EINVAL
        puts "Can't remove current directory"
        return
      rescue Errno::EBUSY
        puts "Can't remove /"
        return
      rescue Errno::ENOTEMPTY
        puts "Can't remove non empty directory"
        return
      rescue Errno::EACCES
        puts "You don't have permissions to remove this"
        return
      end

      rmdir_postconditions(args)
    end
  end


  # Only allow user to do this for one file
  def Commands.filewatch(args)
    if filewatch_validation(args)
      filewatch_preconditions(args)

      delay = args[0].to_i
      file = args[1]
      alteration = args[2]
      action = Proc.new {Commands.send(args[3], args[4..args.length])}

      file_watcher = FileWatcher.new(delay, [file], action, alteration)
      file_watcher.start_watchers

    end
  end

  def Commands.timer(args)
    if timer_validation(args)
      timer_preconditions(args)
      myTimer = ActionTimer.new
      myTimer.sleepMessage(args[0].to_i,args[1].to_i,args[2])
    end
  end

  def Commands.touch(args)
    if touch_validation(args)
      touch_preconditions(args)

      begin
        args.each do |file|
          handle = File.open(file, 'a') {}
        end
      rescue Errno::EISDIR
        puts "Can't touch a directory"
        return
      rescue Errno::EACCES
        puts "You don't have permissions to do that"
        return
      end

      touch_postconditions(args)
    end
  end

end
