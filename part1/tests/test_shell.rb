gem 'test-unit'
require 'test/unit'
require '../../part1/shell'

class TestShell < Test::Unit::TestCase

  def setup
    @shell = Shell.new
  end

  def test_invalid_command
    assert_raises NotImplementedError do
      @shell.execute("cheerios")
    end
  end

  def test_valid_command
    # If echo is no longer a cmd then we'd raise NotImplemented here
    @shell.execute("echo")
  end

end