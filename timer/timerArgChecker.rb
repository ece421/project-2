class TimerArgChecker

  # Validates arguments to timer
	def checkArgs(myArgs)
		if myArgs.length !=3
			self.printHowTo()
			return false
		else	
			seconds = myArgs[0]
			nanoseconds = myArgs[1]
			message = myArgs[2]
			if seconds.to_i.to_s == seconds
				if seconds.to_i < 0
					puts "error: seconds must be greater or equal to zero"
					return false
				end
				if self.numberTooBig?(seconds.to_i)
					puts "error: seconds is too big, max is #{MAX_INT_32}"
				end
			else
				puts "seconds should be a number"
				return false
			end

			if nanoseconds.to_i.to_s == nanoseconds
				if nanoseconds.to_i < 0
					puts "error: seconds must be greater or equal to zero"
					return false
				end
				if self.numberTooBig?(nanoseconds.to_i)
					puts "error: nanoseconds is too big, max is #{MAX_INT_32}"
				end
			else
				puts "error: nanoseconds must be a number"
				return false
			end
		end
    true
	end

  # Displays information on how to use
	def printHowTo
		puts "usage: timer [seconds] [nanoseconds] [string]"
	end

  # Checks if number is too large
	def numberTooBig?(number)
		return number.to_i > MAX_INT_32
	end

end