require '../timer/ActionTimer'
require '../timer/timerArgChecker'
myTimer = ActionTimer.new
argCheck = TimerArgChecker.new
if argCheck.checkArgs(ARGV)
  myTimer.sleepMessage(ARGV[0].to_i,ARGV[1].to_i,ARGV[2])
end