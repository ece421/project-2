require '../timer/actionTimer'
require '../timer/timerArgChecker'
myTimer = ActionTimer.new
argCheck = TimerArgChecker.new
inputArgs = ARGV.clone()
ARGV.clear()
if argCheck.checkArgs(inputArgs)
  myTimer.sleepMessage(inputArgs.shift.to_i,inputArgs.shift.to_i,inputArgs.shift)
end