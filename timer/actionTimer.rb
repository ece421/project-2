require "../timer/timer"
require '../timer/contracts/timer_contracts'

class ActionTimer

  # Sleeps for the specified time
	def sleepTimer(seconds,nanoseconds)
			Timer::sleep(seconds,nanoseconds)
	end

  # Sleeps for the specified time and prints the message
	def sleepMessage(seconds,nanoseconds,message)
		TimerContract::sleepMessagePreConditions(seconds,nanoseconds,message)
		fork do 
			Timer::sleep(seconds,nanoseconds)
			puts ">timer message: #{message}"
		end
	end


end