%module timer
 
%{ 
#include <time.h>
void sleep(int seconds, int nanoseconds);
%} 
/* Parse the function prototypes to generate wrappers */ 
void sleep(int seconds, int nanoseconds);
