gem 'test-unit'
require 'test/unit'
include Test::Unit::Assertions
module TimerContract 

  def self.sleepMessagePreConditions(seconds,nanoseconds, message)
    assert seconds >= 0, "Can't have a negative time in seconds"
    assert nanoseconds >= 0, "Can't have a negative time in nanoseconds"
    assert message.is_a?(String), "Message should be a string"
  end

  def self.sleepTimerPreConditions(seconds,nanoseconds)
    assert @seconds != nil, "seconds must not be nil, how does one sleep for nil seconds"
    assert @seconds >= 0, "seconds can not be negative , how does one sleep for negative seconds"
    assert @nanoseconds >= 0, "nanoseconds can not be negative, really nanosecond sleeping?"
    assert @nanoseconds != nil, "nanoseconds can not be nil duh"


  end

end 
