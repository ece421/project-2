#include <time.h>

/* C implementation of sleep for timer */
void sleep(int seconds , int nanoseconds){
	struct timespec timerRes;
	timerRes.tv_sec = seconds;
	timerRes.tv_nsec = nanoseconds;
	nanosleep(&timerRes , NULL); //second arg is structure to hold remainder if interuptted

}

