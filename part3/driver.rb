require '../part3/file_watcher'

class Driver

  attr_accessor :modify_watcher, :destroy_watcher, :create_watcher

  # Creates sample watchers, before running create destiny.temp and goodbye_my_friend in wd
  def do_stuff
    file_list_modify = ['destiny.tempo']
    file_list_destroy = ['goodbye_my_friend']
    file_list_not_here = ['windows98.exe']
    delay = 1
    action = Proc.new {puts "Just an ordinary day until you came around and flipped my life upside down"}

    @modify_watcher = FileWatcher.new(delay, file_list_modify, action, 'MODIFY')
    @destroy_watcher = FileWatcher.new(delay, file_list_destroy, action, 'DESTROY')
    @create_watcher = FileWatcher.new(delay, file_list_not_here, action, 'CREATE')

    @modify_watcher.start_watchers
    @destroy_watcher.start_watchers
    @create_watcher.start_watchers
  end


end
