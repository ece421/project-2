require '../part3/contracts/file_watcher_contracts'
require '../timer/actionTimer'

class FileWatcher
  include FileWatcherContracts

  attr_reader :duration, :file_list, :action, :alteration, :file_process_hash, :myTimer

  # Creates a FileWatcher with the specified arguments
  def initialize(duration, file_list, action, alteration)
    initialize_file_watcher_preconditions(duration, file_list, action, alteration)
    @duration = duration
    @file_list = file_list
    @action = action
    @alteration = alteration
    @file_process_hash = Hash.new
    @myTimer = ActionTimer.new
    class_invariant
    initialize_file_watcher_postconditions(duration, file_list, action, alteration)
  end

  # For all files in @file_list create a new watcher
  def start_watchers
    start_watchers_preconditions
    class_invariant
    if @alteration.casecmp('create') == 0
      @file_list.each do |file|
        new_create_watcher(file)
      end
    elsif @alteration.casecmp('destroy') == 0
      @file_list.each do |file|
        new_destroy_watcher(file)
      end
    elsif @alteration.casecmp('modify') == 0
      @file_list.each do |file|
        new_modify_watcher(file)
      end
    end
    start_watchers_postconditions
    class_invariant
  end

  # Performs the users action
  def execute_user_action
    class_invariant
    begin
      @action.call
    rescue Errno::EACCES
      puts 'Permission Denied'
    rescue SecurityError
      puts 'Unsafe operation'
    end
    class_invariant
  end

  # Creates a new create file watcher which immediately starts looking for the file to be created
  def new_create_watcher(file)
    new_create_watcher_preconditions(file)
    class_invariant
    child = fork do
      Signal.trap('HUP') {puts "Watcher for #{file} killed"; exit 0}
        begin
          child_watcher_preconditions(file)
          sleep(0) until File.exist?(file)
          puts "File #{file} was created"
          myTimer.sleepTimer(@duration,0)
          # sleep (@duration)
          self.execute_user_action

        rescue IOError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::ENOENT => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::EACCES => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue SecurityError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        end
    end
    Process.detach(child)
    @file_process_hash[file] = child
    class_invariant
    new_watcher_postconditions(file)
  end

  # Watcher will continue watching until file is moved/deleted or watcher is killed
  def new_modify_watcher(file)
    new_modify_watcher_preconditions(file)
    class_invariant

    child = fork do
      Signal.trap('HUP') {puts "Watcher for #{file} killed"; exit 0}

      loop{
        begin
          child_watcher_preconditions(file)
          old_time = File.stat(file).mtime
          sleep(0) until File.stat(file).mtime > old_time
          puts "File #{file} was modified"
          myTimer.sleepTimer(@duration,0)
          # Make the kid do the action, we've gotta see if file changed again!
          grandkid = fork{
            self.execute_user_action
          }
          # Detach to avoid the Walking Dead Sunday Nights on AMC
          Process.detach(grandkid)
        rescue IOError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::ENOENT => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::EACCES => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue SecurityError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        end
      }
    end
    Process.detach(child)
    @file_process_hash[file] = child
    class_invariant
    new_watcher_postconditions(file)
  end

  # Creates new destroy watcher that immediately starts watching for provided file to be destroyed
  def new_destroy_watcher(file)
    new_destroy_watcher_preconditions(file)
    class_invariant

    child = fork do
      Signal.trap('HUP') {puts "Watcher for #{file} killed"; exit 0}

        begin
          child_watcher_preconditions(file)
          sleep(0) until !File.exist?(file)
          puts "File #{file} was deleted"
          myTimer.sleepTimer(@duration,0)
          self.execute_user_action
        rescue IOError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::ENOENT => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue Errno::EACCES => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        rescue SecurityError => e
          puts "Watcher for #{file} killed"
          @file_process_hash.delete(file)
          exit
        end
    end
    Process.detach(child)
    @file_process_hash[file] = child
    class_invariant
    new_watcher_postconditions(file)
  end

  # Kill all of the watchers that were created
  def kill_all_watchers
    kill_all_watchers_preconditions
    class_invariant

    @file_process_hash.each do |file, pid|
      kill_process(pid, file)
    end

    class_invariant
    kill_all_watchers_postconditions
  end

  # Kill the watcher with the specified pid watching file file
  def kill_process(pid, file)
    kill_process_preconditions(pid, file)
    class_invariant

    Process.kill('HUP', pid)
    @file_process_hash.delete(file)

    class_invariant
    kill_process_postconditions(pid, file)
  end

  # kills all watchers for file file
  def kill_watchers_for_file(file)
    kill_watchers_for_file_preconditions(file)
    class_invariant

    kill_process(@file_process_hash[file], file)
    class_invariant

    kill_watchers_for_file_postconditions(file)
  end

end