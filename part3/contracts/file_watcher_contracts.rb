gem 'test-unit'
require 'test/unit'
include Test::Unit::Assertions
module FileWatcherContracts

  def initialize_file_watcher_preconditions(duration, files, action, alteration)
    assert files != nil && duration != nil && alteration != nil, "No nil arguments please"
    assert files.size > 0, "No files provided"
    if alteration.casecmp('create') != 0
      files.each do |file|
        assert File.exist?(file), "file #{file} doesn't exist"
      end
    end
    assert duration >= 0, "Duration should be greater than 0"
    assert action.respond_to?(:call)
    assert alteration.casecmp('create') == 0|| alteration.casecmp('destroy') == 0|| alteration.casecmp('MODIFY') == 0,
           "alteration: #{alteration} was not valid"
  end

  def initialize_file_watcher_postconditions(duration, files, action, alteration)
    assert @duration >= 0, "Can't have a negative duration"
    assert @file_list != nil, "Files can't be nil can they now"
    assert @alteration != nil, "A nil action doesn't make much sense"
    assert @duration >= duration, "Set duration is greater than duration created with"
    assert alteration.casecmp(@alteration) == 0, "Alteration has changed"
  end

  def child_watcher_preconditions(file)
    assert file != nil, "File name not provided"
    assert file.is_a?(String), "File name wasn't a string"
    if @alteration.casecmp("CREATE") == 0
      assert !File.exist?(file), "File already exists"
    else
      assert File.exist?(file), "File doesn't exist"
    end
  end

  def new_create_watcher_preconditions(file)
    assert !File.exist?(file), "File: #{file} already exists"
    new_watcher_preconditions(file)
  end

  def new_modify_watcher_preconditions(file)
    assert File.exist?(file), "File doesn't exist"
    new_watcher_preconditions(file)
  end

  def new_destroy_watcher_preconditions(file)
    assert File.exists?(file), "File doesn't exist"
    new_watcher_preconditions(file)
  end

  def new_watcher_preconditions(file)
    assert !@file_process_hash.has_key?(file), "Watcher already exists for file: #{file}"
  end

  def new_watcher_postconditions(file)
    assert @file_process_hash.include?(file), "File wasn't added to hash"
    assert Process.kill(0, @file_process_hash[file]), "Process wasn't started"
  end

  def start_watchers_preconditions
    assert @alteration.casecmp('create') == 0 || @alteration.casecmp('destroy') == 0||
               @alteration.casecmp('modify') == 0, "Alteration wasn't valid"
  end

  def start_watchers_postconditions
    @file_list.each do |file|
      assert @file_process_hash.has_key?(file), "Watcher wasn't added to hash"
    end
  end

  def kill_all_watchers_preconditions
    assert !@file_process_hash.empty?, "Hash was empty"
  end

  def kill_all_watchers_postconditions
    assert @file_process_hash.empty?, "Hash wasn't empty"
  end

  def kill_process_preconditions(pid, file)
    assert Process.kill(0, pid), "Process to kill isn't running"
    assert @file_process_hash.has_key?(file), "Watcher to kill wasn't in files process hash"
  end

  def kill_process_postconditions(pid, file)
    assert !@file_process_hash.has_key?(file), "File wasn't removed from hash after killing"
  end

  def kill_watchers_for_file_preconditions(file)
    assert @file_process_hash.has_key?(file), "Watcher to kill wasn't in file process hash"
  end

  def kill_watchers_for_file_postconditions(file)
    assert !@file_process_hash.has_key?(file), "File still in hash"
  end

  def class_invariant
    assert @duration >= 0, "Negative duration doesn't work"
    assert @file_list != nil, "Nil files you say, well that doesn't help"
    assert @action.respond_to?(:call), "We can't call our action"
    assert @alteration != nil, "A nil action doesn't mean much"
  end

end
